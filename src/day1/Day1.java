package day1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * Lets get Freqy!
 * Created by CinVerd on 03/12/2018.
 */
public class Day1 {

    public static void main(String[] args) throws IOException {
        //partOne();
        partTwo();
    }

    //Get the current frequency/
    //Answer: 411
    public static void partOne() throws IOException {
        String freqList = "C:\\santadata\\day1input.txt";
        List<String> frequencies = Collections.emptyList();
        //make sure nothing untoward is happening
        frequencies = Files.readAllLines(Paths.get(freqList));
        int frequencyCount= 0;
        for(String freq: frequencies){
            //do the thing
            System.out.println(freq);
            frequencyCount += Integer.parseInt(freq);
        }

        System.out.println(frequencyCount);

    }

    //Determine first frequency that's reached twice.
    //Answer: 56360
    public static void partTwo() throws IOException {
        String freqList = "C:\\santadata\\day1input.txt";
        List<String> frequencies = Collections.emptyList();

        //Read the list once
        frequencies = Files.readAllLines(Paths.get(freqList));

        //Get the list a few times, we may need to loop through it multiple times
        for(int i = 0; i<1000; i++){
            frequencies.addAll(Files.readAllLines(Paths.get(freqList)));
        }

        HashSet<Integer> set = new HashSet<Integer>();
        //Keep track of the length of the hashset
        int lastHashCount = 0;

        //Count frequencies as before
        int frequencyCount= 0;

        for(String freq: frequencies){
            lastHashCount = set.size();
            frequencyCount += Integer.parseInt(freq);
            set.add(frequencyCount);

            //Where the magic happens
            //If current count is the same as last, frequency didn't get added to the list, therefore duplicate.
            if(lastHashCount == set.size()){
                System.out.println("First duplicate frequency: " + frequencyCount);
                //Piggyback on IO exception we're already throwing to end loop
                throw new IOException();
            }
        }
        System.out.println("Shouldn't have reached this point");

    }
}
