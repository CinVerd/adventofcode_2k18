package day2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 *
 * Created by CinVerd on 10/12/2018.
 */
public class Day2 {

    public static void main(String[] args) throws IOException {
        System.out.println("{++++} Operation Save Christmas Day 2");
        part1();
    }

    //Generate checksum to ensure all packages scanned
    //Find ids with exactly 2 duplicate letters, and exactly three duplicate letters
    //Answer is 7163
    public static void part1() throws IOException{
        List<String> boxIDs = readFile();
        int numberOfDuplicates = 0;
        int numberOfTriplicates = 0;
        for(String id: boxIDs){
            int previousLength = id.length();

            //Need to get non duplicated list of unique characters in id
            //Old friend hashset to the rescue!
            HashSet<Character> set = new HashSet<Character>();
            for(Character character : id.toCharArray()){
                set.add(character);
            }
            Character[] placeholder = new Character[1];
            placeholder = set.toArray(placeholder);
            //Created list of unique characters

            //only count first dupe and first trip for each id
            Boolean duplicateCounted = false;
            Boolean triplicateCounted = false;

            //Loop through the id with each unique character, determine how many times it shows up
            for(Character character:placeholder){
                id = id.replaceAll(character.toString(), "");
                int lengthDifference = previousLength - id.length();
                if(lengthDifference == 2 && !duplicateCounted){
                    numberOfDuplicates++;
                    duplicateCounted = true;
                }else if (lengthDifference == 3 && !triplicateCounted){
                    numberOfTriplicates++;
                    triplicateCounted = true;
                }

                previousLength = id.length();
            }
        }
        int checksum = numberOfDuplicates * numberOfTriplicates;
        System.out.println("Number of duplicates: " + numberOfDuplicates);
        System.out.println("Number of triplicates: " + numberOfTriplicates);
        System.out.println("Done looping, checksum: " + checksum);
    }

    public static void part2() throws IOException {
        List<String> boxIDs = readFile();
        
    }

    //Iterative improvement!
    private static List<String> readFile() throws IOException{
        String freqList = "C:\\santadata\\day2input.txt";
        return Files.readAllLines(Paths.get(freqList));
    }
}
